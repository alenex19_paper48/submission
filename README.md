# Lomuto BlockQuicksort Experiments 

Source code, raw experimental results, and Jupyter notebook used in the paper *Simple and Fast BlockQuicksort using Lomuto�s Partitioning Scheme* by Martin Aum�ller and Nikolaj Hass, to be presented at ALENEX 2019.

## Included Algorithms

All algorithms are header-only in [src/algorithms/](src/algorithms/):

## Dependencies

Needs g++, cmake in version >= 2.8, libboost-random and libpapi for performance
measurements. (Enabled by default, can be changed in CMakeLists.txt.)

## How to build

Use the following commands on the top-level directory of the project.

> mkdir build; cd build
> cmake ..
> make

For a release build with optimization flags, use 
> cmake -DCMAKE_BUILD_TYPE=Release

After successful compilation, the executable is located at
build/src/benchmark.

## Example Calls

Run all algorithms on permutation input with 2^23,..., 2^27 elements, repeat each input size 10 times.

> src/benchmark -s 128m -S 1g -R 10 permutation

## Raw results and Notebook

See directory [notebook_and_raw_results](notebook_and_raw_results/) for the [Jupyter notebook](notebook_and_raw_results/Lomuto%20Experiments.ipynb) containing calculations and plots. Please unzip the `raw_results.tar.gz` first, which contains all experimental results that have been obtained through experiments.

## Contact

Write an email to Martin (maau@itu.dk).


