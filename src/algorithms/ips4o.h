#include <algorithm>
#include "ips4o/ips4o.hpp"

namespace ips4o {

template <typename ValueType>
void sort1()
{
    assert( g_input_size % sizeof(ValueType) == 0 );

    ValueType* input = (ValueType*)g_input;
    size_t n = g_input_size / sizeof(ValueType);

    ips4o::sort(input, input + n);
}

CONTESTANT_REGISTER_ALL(sort1, "ips4o", "ips4o")

} 
