#include <algorithm>
#include <iostream>

namespace qsort3lomutoblock {

#define QSORT3BLOCKSIZE 1024

template <typename Iterator>
void sort(Iterator lo, Iterator hi)
{
    typedef typename std::iterator_traits<Iterator>::value_type value_type;
    typedef typename std::iterator_traits<Iterator>::difference_type index;
    
    if (lo + 23 >= hi) {
        return InsertionSort(lo, hi - lo + 1);
    } else if (lo + 1000 < hi) {
        pivot_strategies::medians_first_three_of_five(lo, hi);	
    } else {
        pivot_strategies::first_three_of_five(lo, hi);
    }

    index block[QSORT3BLOCKSIZE];

    const value_type p = *lo;
    const value_type q = *(lo + 1);
    const value_type r = *hi;

    Iterator counter = lo + 2, offset1 = lo + 2, offset2 = lo + 2, offset3 = lo + 2;
    short num1 = 0, num2 = 0, num3 = 0;

    while (counter < hi) {
        auto t = std::min((hi - counter), (long) QSORT3BLOCKSIZE);
        for (index j = 0; j < t; ++j) {
            block[num3] = j;
            num3 += (r >= counter[j]);
        }
        for (index j = 0; j < num3; ++j) {
            std::iter_swap(offset3 + j, counter + block[j]);
        }

        for (index j = 0; j < num3; ++j) {
            block[num2] = j;
            num2 += (q >= offset3[j]);
        }
        for (index j = 0; j < num2; ++j) {
            std::iter_swap(offset2 + j, offset3 + block[j]);
        }

        for (index j = 0; j < num2; ++j) {
            block[num1] = j;
            num1 += (p > offset2[j]);
        }
        for (index j = 0; j < num1; ++j) {
            std::iter_swap(offset1 + j, offset2 + block[j]);
        }
        offset3 += num3;
        offset2 += num2;
        offset1 += num1;
        num1 = 0;
        num2 = 0;
        num3 = 0;
        counter += t;
    }

    rotations::rotate3(*(lo + 1), *(offset1 - 1), *(offset2 - 1));
    std::iter_swap(lo, offset1 - 2);
    std::iter_swap(offset3, hi);
    
    sort(lo, offset1 - 3);
    if (p != q) {
        sort(offset1 - 1, offset2 - 2);
    }
    if (q != r) {
	sort(offset2, offset3 + 1);
    }
    sort(offset3, hi);
}

template <typename ValueType>
void qsort()
{
    assert( g_input_size % sizeof(ValueType) == 0 );

    ValueType* input = (ValueType*)g_input;
    size_t n = g_input_size / sizeof(ValueType);

    sort(input, input + n - 1);
}

CONTESTANT_REGISTER_ALL(qsort, "qsort3lomutoblock", "Quicksort, triple pivot, lomuto, block")

} 
