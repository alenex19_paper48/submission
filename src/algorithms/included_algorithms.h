#define BLOCKSIZE 1024
#include "pivot_strategies.h"
#include "stdsort.h"
#include "ips4o.h"
#include "qsort1block.h"
#include "qsort2block.h"
#include "qsort3block.h"
#include "qsort1hoareblock_optimized.h"
