#include <algorithm>
#include <iostream>

namespace qsort1lomutoblock {

#define QSORT1BLOCKSIZE 1024

template <typename Iterator>
void sort(Iterator lo, Iterator hi)
{
    typedef typename std::iterator_traits<Iterator>::value_type value_type;
    typedef typename std::iterator_traits<Iterator>::difference_type index;

    index block[QSORT1BLOCKSIZE];
    Iterator pivot_position;
    
	if (lo + 16 >= hi) {
        return InsertionSort(lo, hi - lo + 1);
    }
	else if (lo + 100 < hi) {
	    pivot_position = pivot_strategies::median_of_5_medians_of_5(lo, hi);
    }
    else {
        pivot_position = pivot_strategies::median_of_3(lo, hi);
    }
	    

    std::iter_swap(pivot_position, hi);
    pivot_position = hi;
    const value_type pivot = *hi;

    Iterator counter = lo, offset = lo;
    int num_left = 0;

    while (counter < hi) {
        auto t = std::min(hi - counter, (index) QSORT1BLOCKSIZE);
        for (index j = 0; j < t; ++j) {
            block[num_left] = j;
            num_left += (pivot > counter[j]);
        }
        for (index j = 0; j < num_left; j++) {
            std::iter_swap(offset, counter + block[j]);
            offset++;
        }
        num_left = 0;
        counter += t;
    }
    
    std::iter_swap(offset, pivot_position);
    
    sort(lo, offset - 1);
    sort(offset + 1, hi);
}

template <typename ValueType>
void qsort1()
{
    assert( g_input_size % sizeof(ValueType) == 0 );

    ValueType* input = (ValueType*)g_input;
    size_t n = g_input_size / sizeof(ValueType);

    sort(input, input + n - 1);
}

CONTESTANT_REGISTER_ALL(qsort1, "qsort1lomutoblock", "Quicksort, single pivot, lomuto, block")

} 
