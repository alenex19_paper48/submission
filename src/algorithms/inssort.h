template <typename Iterator>
static inline
void InsertionSort(Iterator A, ssize_t n)
{
    typedef typename std::iterator_traits<Iterator>::value_type value_type;

    for (ssize_t i = 1; i < n; ++i)
    {
        value_type tmp, key = A[i];
//        g_assignments++;

        ssize_t j = i - 1;
        while (j >= 0 && (tmp = A[j]) > key)
        {
            A[j + 1] = tmp;
//            g_assignments++;
            j--;
        }
        A[j + 1] = key;
//        g_assignments++;
    }
}
