#include <algorithm>

namespace stdsort {

template <typename ValueType>
void sort1()
{
    assert( g_input_size % sizeof(ValueType) == 0 );

    ValueType* input = (ValueType*)g_input;
    size_t n = g_input_size / sizeof(ValueType);

    std::sort(input, input + n);
}

CONTESTANT_REGISTER_ALL(sort1, "stdsort", "std::sort Introsort")

} // namespace stdsort
