#include <algorithm>
#include <iostream>

namespace qsort2lomutoblock {

#define QSORT2BLOCKSIZE 1024

template <typename Iterator>
void sort(Iterator lo, Iterator hi)
{
    typedef typename std::iterator_traits<Iterator>::value_type value_type;
    typedef typename std::iterator_traits<Iterator>::difference_type index;
    
    if (lo + 23 >= hi) {
        return InsertionSort(lo, hi - lo + 1);
    }
    else if (lo + 1000 < hi) {
        pivot_strategies::medians_first_third_of_five(lo, hi);
    }
    else if (lo + 200 <  hi) {
        pivot_strategies::first_third_of_five(lo, hi);
    }
    else {
        pivot_strategies::first_two_of_three(lo, hi);
    }

    index block[QSORT2BLOCKSIZE];

    const value_type p = *lo;
    const value_type q = *hi;

    Iterator counter = lo + 1, offset1 = lo + 1, offset2 = lo + 1;
    short num1 = 0, num2 = 0;

    while (counter < hi) {
        auto t = std::min(hi - counter, (index)QSORT2BLOCKSIZE);
        for (index j = 0; j < t; ++j) {
            block[num2] = j;
            num2 += (q >= counter[j]);
        }
        for (index j = 0; j < num2; ++j) {
            std::iter_swap(offset2 + j, counter + block[j]);
        }
        counter += t;
        for (index j = 0; j < num2; ++j) {
            block[num1] = j;
            num1 += (p > offset2[j]);
        }
        for (index j = 0; j < num1; ++j) {
            std::iter_swap(offset1++, offset2 + block[j]);
        }
        offset2 += num2;
        num1 = 0;
        num2 = 0;
    }

    std::iter_swap(lo, offset1 - 1);
    std::iter_swap(offset2, hi);
    
    sort(lo, offset1 - 2);
    if (p != q) {
        sort(offset1, offset2 - 1);
    }
    sort(offset2 + 1, hi);
}

template <typename ValueType>
void qsort()
{
    assert( g_input_size % sizeof(ValueType) == 0 );

    ValueType* input = (ValueType*)g_input;
    size_t n = g_input_size / sizeof(ValueType);

    sort(input, input + n - 1);
}

CONTESTANT_REGISTER_ALL(qsort, "qsort2lomutoblock", "Quicksort, dual pivot, lomuto, block")

} 
