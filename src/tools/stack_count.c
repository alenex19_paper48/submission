#include "stack_count.h"

#include <inttypes.h>
#include <stdio.h>

static const size_t stacksize = 6*1024*1024;

/* "clear" the stack by writing a sentinel value into it. */
void* stack_count_clear(void)
{
    const size_t asize = stacksize / sizeof(uint32_t);
    uint32_t stack[asize]; // allocated on stack
    uint32_t* p = stack;
    while( p < stack + asize ) *p++ = 0xDEADC0DEu;
    return p;
}

/* checks the maximum usage of the stack since the last clear call. */
size_t stack_count_usage(void* lastbase)
{
    const size_t asize = stacksize / sizeof(uint32_t);
    uint32_t* p = (uint32_t*)lastbase - asize; // calculate top of last clear.
    while ( *p == 0xDEADC0DEu ) ++p;
    return ((uint32_t*)lastbase - p) * sizeof(uint32_t);
}

/*****************************************************************************/
