#ifndef _STACK_COUNT_H_
#define _STACK_COUNT_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" { /* for inclusion from C++ */
#endif

/* "clear" the stack by writing a sentinel value into it. */
extern void* stack_count_clear(void);

/* checks the maximum usage of the stack since the last clear call. */
extern size_t stack_count_usage(void* lastbase);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _STACK_COUNT_H_ */

/*****************************************************************************/
