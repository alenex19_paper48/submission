#ifndef _MALLOC_COUNT_H_
#define _MALLOC_COUNT_H_

#include <stdlib.h>

#ifdef __cplusplus
extern "C" { /* for inclusion from C++ */
#endif

/* returns the currently allocated amount of memory */
extern size_t malloc_count_current(void);

/* returns the current peak memory allocation */
extern size_t malloc_count_peak(void);

/* resets the peak memory allocation to current */
extern void malloc_count_reset_peak(void);

/* typedef of callback function */
typedef void (*malloc_count_callback_type)(void* cookie, size_t current);

/* supply malloc_count with a callback function that is invoked on each change
 * of the current allocation. The callback function must not use
 * malloc()/realloc()/free() or it will go into an endless recurive loop! */
extern void malloc_count_set_callback(malloc_count_callback_type cb, void* cookie);

/* user function which prints current and peak allocation to stderr */
extern void malloc_count_print_status(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _MALLOC_COUNT_H_ */

/*****************************************************************************/
