# CMake file for lomuto speedtest project

project(lomuto)

cmake_minimum_required(VERSION 2.8)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

option(WITH_MALLOC_COUNT "Use malloc_count for memory profiling" OFF)

option(WITH_PAPI "Use PAPI library for performance counting" OFF)

# disallow in-source builds

if("${PROJECT_SOURCE_DIR}" STREQUAL "${PROJECT_BINARY_DIR}")
   message(SEND_ERROR "In-source builds are not allowed.")
endif("${PROJECT_SOURCE_DIR}" STREQUAL "${PROJECT_BINARY_DIR}")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

# Enable warnings

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -W -Wall")

# Save Assembler Code

# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -S")

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3 -march=native -funroll-loops")

find_package(Boost COMPONENTS random REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})

# Find PAPI library

if(WITH_PAPI)

  find_path(PAPI_INCLUDE_DIRS papi.h
    PATHS /usr/include/ /usr/local/include/)

  find_library(PAPI_LIBRARIES NAMES papi
    PATHS /usr/lib/ /usr/local/lib/)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(PAPI DEFAULT_MSG
    PAPI_INCLUDE_DIRS
    PAPI_LIBRARIES
    )

endif()

# descend into source
add_subdirectory(src)
